

import java.util.Scanner;
import java.util.ArrayList;

public class GraduationPlanner {

    public static void main(String[] args) {

  //USER INPUT PLANNED CUs PER TERM
        
  ArrayList<Double> compUnits = new ArrayList<Double>();
  Scanner in = new Scanner(System.in);
       
  totalCompUnits(in, compUnits);   
       
  double sumOfCUs = totalCUs(compUnits);   
    

// USER INPUT : CUs PER TERM TO COMPLETE 
    
     Scanner input2 = new Scanner(System.in);
     
     System.out.println("Please enter how many CUs per term you would like to complete, press enter when finished:    ");
      double plannedCUs = input2.nextDouble();
      if (plannedCUs < 12) {
          System.out.println("Error. You must be enrolled in a minimum of 12 competency units per term.\n Program exiting, please try again.");
          System.exit(0);
      }
    else {
       System.out.println("Thank you for your input!");
    }
    
    //SYSTEM OUTPUT  

    double termsLeft = Math.ceil(sumOfCUs / plannedCUs);

    System.out.printf("You got %.0f term(s) left to complete your degree \n", + termsLeft);

    final double TUITION = 2890;

    double tuitionLeft = termsLeft * TUITION;

    System.out.printf("The estimated cost is $ %.0f \n", + tuitionLeft );

    final double MONTHS_PER_TERM = 6;

    double monthsLeft = termsLeft * MONTHS_PER_TERM;

    System.out.printf("You will complete your degree in %.0f months.\n", + monthsLeft); 


        } 

        //END MAIN METHOD

        //STATIC METHODS
    
    public static double totalCUs(ArrayList<Double> compUnits){
    double sum = 0;
    for (double element : compUnits) {
        sum = sum + element;
    }
    return sum;
    }
    
    public static void totalCompUnits (Scanner in, ArrayList<Double> compUnits ) {
    System.out.println("Please enter the competency units (CUs) remaining to complete your degree (for example 3 4 2 4 2), press any letter when finished:  ");
    while (in.hasNextDouble()) {
    double competencyUnits = in.nextDouble();
    if (competencyUnits < 1 || competencyUnits > 9) {
                System.out.println("The course has a miminum value of 1 CU, max 9 CUs. Application exits, please restart and re-enter valid comptency units.");
                System.exit(0);
            }
        else {
                compUnits.add(competencyUnits);
            }
        }
    }
    
}